import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Box extends Component {
  render() {
    return (
      <div style={{ border: '1px solid #09f', margin: 5, padding: 5 }}>
        { this.props.children }
      </div>
    )
  }
}


class Article extends Component {
  static propTpyes = {
    author: PropTypes.string.isRequired
  }
  // constructor (props) {
  //   super(props)
  //   if (typeof props.author === 'undefined') {
  //     console.warn('author prop is required')
  //     throw new Error('author prop is required')
  //   }
  // }

  render() {
    const { author, children, date, title } = this.props

    return (
      <section style={{ borderBottom: '1px solid #000', marginBottom: 50}}>
        <h2>{title}</h2>
        {author && <p><em>Escrito por {author}</em></p>}
        <Box>{date}</Box>
        <article>
          {children}
        </article>
      </section>
    )
  }
}

// Article.propTpyes = {
//   author: PropTypes.string
// }

class App extends Component {
  render () {
    return (
      <div className="App">
        <h4>Children props</h4>
        {/* <Box>Hola! Soy un children!</Box>
        <Box>Yo soy otro children</Box> */}
        <Article 
          author='Miguel Valdés'
          date={new Date().toLocaleDateString()}
          title='Articulo sobre la prop children'
        >
          <p>El contenido que envolvemos dentro del componenete Article será enviado al componenete como this.props.children.</p>
          <strong>Y mantiene las etiquetas y componentes que hayáis añadido dentro</strong>
        </Article>
        <Article 
          author='Miguel Valdés'
          date={new Date().toLocaleDateString()}
          title='Otro Articulo'
        >
          <p>El contenido que envolvemos dentro del componenete Article será enviado al componenete como this.props.children.</p>
          <strong>Y mantiene las etiquetas y componentes que hayáis añadido dentro</strong>
        </Article>
        <Article 
          author='Miguel Valdés'
          date={new Date().toLocaleDateString()}
          title='Articulo 3'
        >
          <p>El contenido que envolvemos dentro del componenete Article será enviado al componenete como this.props.children.</p>
          <strong>Y mantiene las etiquetas y componentes que hayáis añadido dentro</strong>
        </Article>
      </div>
    )
  }
}

export default App;