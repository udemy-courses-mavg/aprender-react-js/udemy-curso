import React, { Component } from 'react';
// import ComponentWillMount from './sections/life-cycle/componentWillMount.js'
// import Render from './sections/life-cycle/render.js'
// import ComponentDidMount from './sections/life-cycle/componentDidMount.js'
import Fetch from './sections/fetch.js'

class App extends Component {
  render () {
    // console.log('render')
    return (
      <div className="App">
        {/* <ComponentWillMount /> */}
        {/* <Render /> */}
        {/* <ComponentDidMount /> */}
        <Fetch />
      </div>
    )
  }
}

export default App;