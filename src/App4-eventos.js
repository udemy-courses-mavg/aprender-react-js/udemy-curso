import React, { Component } from 'react';

// ANTIGUAMENTE HTML
// <button onclcik="alert('hi there')">...
// EN JSX
// <button onClick={() => alert('Hi here!')}>Hi there!</button>
class App extends Component {
  // constructor () {
  //   super()
  //   this.state = { mouseX: 0, mouseY:0 }
  //   this.handleMouseMove = this.handleMouseMove.bind(this)
  // }

  state = { mouseX: 0, mouseY: 0}
  
  /* TypeError: Cannot read property 'setState' of undefined 
  *  ERROR. Creemos que al apuntar a this el contexto se ejecuta en el contexto del componente y podremos actualizar 
  su State, pero al ejecutarse en respuesta a un evento del navegador el contexto no será ese y no contendra la propiedad "setState"
  *  Resolución del error. Tenemos que asegurarnos que el contexto en el que se ejecuta es el que esperamos y para eso se debe enlazar el 
  contexto al método. Existen dos formas sencillas de resolverlo **:
      * La primera consiste en enlazar al método que controla la evento en el constructor del componente utilizando el método bind de JS.
      Este método devuelve el mismo método pero con el contexto correcto
      * La segunda es utilizar una around function en el método de la clase, ya que las around function enlazan siempre el contexto desde donde
      se declaran.   
  33. Eventos soportados
  */
  // ** MÉTODO BIND (.bind()). Devuelve el mismo método pero con el contexto correcto
  // handleMouseMove = this.handleMouseMove.bind(this) 



  handleClick (e) {
    console.log("EVENTO: ")
    console.log(e)
    console.log("EVENTO NATIVO: ")
    console.log(e.nativeEvent)
    alert('Hi here!!')
  }

  // ** AROUND FUNCTION
  handleMouseMove = (e) => {
    const { clientX, clientY } = e
    this.setState({ mouseX:  clientX, mouseY: clientY })
  }

  // handleMouseMove (e) {
  //   const { clientX, clientY } = e
  //   this.setState({ mouseX:  clientX, mouseY: clientY })
  // }

  render () {
    return (
      <div className="App">
        <h4>Eventos</h4>
        {/* <button onClick={() => alert('Hi here!')}>Hi there!</button> */}
        <button onClick={this.handleClick}>Hi there!</button>
        <div 
          onMouseMove={this.handleMouseMove}
          style={{ border: '1px solid #000', marginTop: 10, padding: 10 }}>
          <p>{this.state.mouseX}, {this.state.mouseY}</p>
        </div>
      </div>
    )
  }
}

export default App;