import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

/* CLASSIC FUNCTION */
// function Hello (props) {
//   return <h2>{ props.title }</h2>
// }

/* AROUND FUNCTION */
// const Hello = (props) => <h2>{props.title}</h2>

/* CLASS */
class Hello extends Component {
  render () {
    return <h2>{this.props.title}</h2>
  }
}

class Text extends Component{
  render () {
    // const textFromBool = this.props.boolean ? 'Value is true' : 'Value is false';
    // const textFromBool = this.props.isActivated ? 'On' : 'Off';
    // const mappedNumbers = this.props.arrayOfNumbers.map(n => n * 2);
    const {
      arrayOfNumbers,
      multiply,
      objectWithInfo,
      title
    } =  this.props;

    // this.props.title = <h3>Comprobando si se puede mutar el valor de title</h3>
    const mappedNumbers = this.props.arrayOfNumbers.map(multiply);

    return (
    /* No se pueden crear dos elementos en línea. El método render() siempre debe devolver un solo elemento y este puede tener tantos 
    subelementos como necesitemos. Por eso lo elementos párrafo (<p>) se envuelven en una etiqueta <div> para solo tener un elementp 
    */ 
    
    // <div>
    //   <p>{this.props.text}</p>
    //   <p>{this.props.number}</p>
    //   <p>{textFromBool}</p>
    //   {/* Para mostrar booleanos por el render se hace de la siguiente manera */}
    //   <p>{JSON.stringify(this.props.boolean)}</p>
    //   <p>{this.props.arrayOfNumbers}</p>
    //   <p>{this.props.arrayOfNumbers.join(', ')}</p>
    //   <p>{mappedNumbers.join(', ')}</p>
    // </div>
    // <div>
    //   <p>{this.props.objectWithInfo.key}</p>
    //   <p>{this.props.objectWithInfo.key2}</p>
    // </div>
    // <div>
    //   <p>{this.props.objectWithInfo.key}</p>
    //   <p>{this.props.objectWithInfo.key2}</p>
    //   <p>{mappedNumbers.join(', ')}</p>
    //   <p>{this.props.arrayOfNumbers.join(', ')}</p>
    // </div>
      <div>
        {title}
        <p>{objectWithInfo.key}</p>
        <p>{objectWithInfo.key2}</p>
        <p>{arrayOfNumbers.join(', ')}</p>
        <p>{mappedNumbers.join(', ')}</p>
        <p></p>
      </div>

    )
  }
}

class App extends Component {
  render () {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Hello title='Hello world!!'/>
          {/* <Text number={2} text='Texto en String' boolean={true}/> */}
          {/* <Text number={2} text='Text 1' boolean arrayOfNumbers={[1, 3, 5]}/>
          <Text 
            arrayOfNumbers={[2, 4, 6]}
            isActivated
            number={2} 
            text='Text 2'
          /> */}
          {/* <Text objectWithInfo={{ key: 'First Value', key2: 'other Value'}}/> */}
          <Text 
            arrayOfNumbers={[2, 4, 6]}
            multiply={( number ) => number * 4} 
            objectWithInfo={{ key: 'First Value', key2: 'Second Value'}} 
            title={<h1>Este es el titulo</h1>}
          />
        </header>
      </div>
    );
  }
}

export default App;
