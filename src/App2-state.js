import React, { Component } from 'react';

// class Title extends Component {
//   render () {
//     return <h1>{this.props.text}</h1>
//   }
// }

// Title.defaultProps = {
//   text: 'Título por defecto'
// }

class Contador extends Component {
  // INCIALIZACIÓN DEL STATE
    // USANDO Class Properties
  /* Para añadir el state al componente se debe añadir el constructor a la clase y dentro se debe llamar el método super() que ejecutará 
  el método constructor de la clase que extendemos (Component)
  */
  constructor(props) {
    super(props)
    // Se inicia el state del componente, se útliza el "this" que es el contexto del componente
    this.state = { contador: this.props.contadorInicial }
    setInterval(() => {
      this.setState({ contador: this.state.contador + 1})
    }, 1000)
  }

    // USANDO PROPOSICIÓN "Class Fields" 
  // state = {contador: 0};

  render() {
    return <ContadorNumero numero={this.state.contador} />
  }
}

Contador.defaultProps = {
  contadorInicial: 0
}

class ContadorNumero extends Component {
  render() {
    // console.log('ContadorNumero render()')
    return <span>{this.props.numero}</span>
  }
}

class App extends Component {
  render () {
    return (
      <div className="App">
        {/* <Title text='Otro título'/> */}
        <p>Primer componente con state</p>
        <Contador contadorInicial={100}/>
      </div>
    )
  }
}

export default App;