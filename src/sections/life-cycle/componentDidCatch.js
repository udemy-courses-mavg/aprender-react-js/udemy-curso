import React, { Component } from 'react'

class BotonQueLanzaError extends Component {
  state = { throwError: false }

  handleSubmit = (e) => {
    e.preventDefault()
    this.setState({ throwError: true })
  }

  render() {
    if (this.state.throwError)
      throw new Error('Error lanzado por el botón')
    return (
      // <button onClick = {() => { this.setState({ throwError: true })}}>
      <button onClick={this.handleSubmit} >
        Lanzar error
      </button>
    )
  }
}

class ComponentDidCatch extends Component {
  state = { hasError: false, errorMsg: '' }
  
  componentDidCatch(error, infoError) {
    console.log('componentDidCatch');
    console.log(error, infoError)
    this.setState({ hasError: true, errorMsg: error.toString() })
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.setState({ hasError:false })    
  }

  render() {
    if (this.state.hasError) {
      return (
        <div>
          <p>Error en el componente: {this.state.errorMsg}</p>
          <button onClick={this.handleSubmit}>
            Volver a la App
          </button>
        </div>
      )
    }

    return (
      <div>
        <h4>Ciclo de error: componentDidCatch</h4>
        <BotonQueLanzaError />
      </div>
    )
  }
}

export default ComponentDidCatch