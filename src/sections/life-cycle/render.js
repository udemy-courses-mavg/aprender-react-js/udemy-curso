import React, { Component } from 'react';

const HelloRender = (props) => <h1>Hola render!</h1>

class Render extends Component {
  constructor(props) {
    console.log('constructor')
    super(props)
    this.state = { mensaje: 'mensaje inicial'}
    // this.state = { mensaje: 'mensaje inicio'}
  }

  compenentWillMout() {
    console.log('componentWillMout')
  }

  render() {
    console.log('render')
    // return 'Soy el método render'
    // return null
    // return this.state.mensaje === 'mensaje inicial' ? 'Yeah!' : null
    // FRAGMENTOS
    return [
      <h1 key='A'>Primer elemento</h1>
      <HelloRender key='B' />
      <HelloRender key='C' />
      <h1 key='D'>Último elemento</h1>
    ]
  }
}

export default Render