import React, { Component } from 'react';

class ComponentDidMount extends Component {
  constructor(props) {
    console.log('constructor')
        super(props)
    this.state = { scroll: 0 }
    // debugger
  }

  componentWillMount() {
    console.log('componentWillMount')
    // debugger
  }

  componentDidMount() {
    console.log('componentDidMount')
    // debugger
    // this.setState({ otroState: '' })
    // document.addEventListener('scroll', function () {
    document.addEventListener('scroll', () => {
      // console.log(window.scrollY)
      this.setState( { scroll: window.scrollY })
    })
  }

  render() {
    console.log('render')
    // debugger
    return (
      <div>
        <h4>Ciclo de montaje: componentDidMount</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium in nibh id fringilla. Ut condimentum nibh nisi, sit amet auctor lectus cursus ac. Aenean tincidunt viverra libero a blandit. Etiam lacinia vitae arcu id sollicitudin. Quisque molestie metus non sapien imperdiet congue. Mauris vel diam venenatis, porta lectus in, scelerisque dui. Fusce nec nunc egestas, rutrum nulla ac, accumsan risus. Morbi faucibus leo nec est aliquet tincidunt. Cras mattis tellus at tellus tristique, id bibendum mi sagittis. Aliquam massa tortor, tempor non mauris a, convallis maximus dui. Proin vel massa aliquam, elementum est pellentesque, condimentum dolor.</p>
        <p>Nam hendrerit purus sed auctor efficitur. Nullam consectetur congue ligula eget pretium. In ac velit iaculis, tincidunt urna sollicitudin, ullamcorper arcu. Aliquam in molestie tellus. Aenean molestie ornare nisl eget tincidunt. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut egestas sem ac ornare pellentesque.</p>
        <p><strong>El scroll es {this.state.scroll}</strong></p>
        <p>Integer faucibus nisl quis ligula pretium dapibus. Mauris malesuada lacus a nulla viverra volutpat. Morbi scelerisque enim nec justo efficitur, nec pharetra diam tempus. Quisque nisl mauris, vehicula vel ante eget, ornare tincidunt magna. Sed sed posuere diam. In hac habitasse platea dictumst. Integer pharetra nisi at magna consectetur, at pulvinar massa convallis. Integer in placerat libero. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer dapibus cursus congue. Nulla ligula urna, consequat sodales placerat ac, condimentum quis velit.</p>
        <p>Phasellus nec dui scelerisque, malesuada risus ullamcorper, aliquet lectus. Vivamus faucibus ultricies tristique. Cras vel sem vitae mi gravida placerat a sed est. Nulla pharetra fringilla purus, id iaculis dolor lobortis eget. Maecenas auctor vestibulum condimentum. Quisque vitae velit eu arcu volutpat tincidunt. Suspendisse aliquam blandit erat, nec porta eros. Nam felis dui, vestibulum et consequat a, dictum sit amet urna. Aenean consectetur ornare finibus. Sed nec pellentesque massa, vitae ultrices sapien. Ut efficitur porttitor sapien sit amet commodo. Nunc fermentum magna vel volutpat eleifend.</p>
        <p>Ut nisi nunc, lacinia id ornare at, semper vitae augue. Mauris non mi nec lorem porttitor vestibulum in nec justo. Aliquam suscipit diam eget eros iaculis, luctus pharetra lacus porttitor. Donec nec arcu ipsum. Donec ullamcorper tellus ligula, laoreet eleifend augue sagittis at. Aliquam in felis vel mi malesuada ornare. Duis orci magna, sagittis eget eros ac, mollis vehicula nisi. Cras ut erat a mauris luctus laoreet et sed elit. Cras ac faucibus mauris. Donec dui dui, auctor eu vehicula eget, auctor id libero. Cras aliquet ultrices risus, ac volutpat metus gravida id. Praesent aliquet porta odio vel efficitur. Nam maximus molestie mollis. Vestibulum eget nisi urna.</p>
      </div>
    )
  }
}

export default ComponentDidMount