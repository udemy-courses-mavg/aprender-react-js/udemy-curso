import React, { Component } from 'react';

class Fetch extends Component {
  state = { bpi: {} }

  componentDidMount() {
    fetch('https://api.coindesk.com/v1/bpi/currentprice.json')
      .then(res => res.json())
      .then(data => {
        // console.log(data)
        const { bpi } = data
        this.setState({ bpi })
      })
  }

  _renderCurrencies() {
    // console.log(this.state.bpi)
    const { bpi } = this.state
    const currencies = Object.keys(bpi) // ['EUR', 'GBP', 'USD'] 
    return currencies.map(currency => (
      <div key={currency}>
        1 BTC is {bpi[currency].rate}
      <span>{currency}</span>
      </div>
    ))
  }
    // return Object.keys(bpi) // ['EUR', 'GBP', 'USD'] 
    //              .map(currency => {
    //                return (
    //                  <div key={currency}>
    //                   1 BTC is {bpi[currency].rate}
    //                   <span>{currency}</span>
    //                  </div>
    //                )
    //              })

  render() {
    return (
      <div>
        <h4>Fetch example</h4>
        <h3>Bitcoin Price Index</h3>
        {this._renderCurrencies()}
      </div>
    )
  }
}

export default Fetch