import React from 'react';


function  RenderCurrencies(props) {
    const { bpi } = props
    const currencies = Object.keys(bpi) 

    return currencies.map(currency => (
      <div key={currency}>
        1 BTC is {bpi[currency].rate}
      <span>{currency}</span>
      </div>
    ))
  }

function BitCoinPrice(props) {
  return (
    <div>
      <h3>Bitcoin Price Index</h3>
      {RenderCurrencies(props)}
    </div>
  )
}

export default BitCoinPrice 