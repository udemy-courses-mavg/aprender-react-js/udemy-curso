/* FUNCIÓN PURA
Siempre devuelve el mismo resultado para los mismos parametros de entrada.
La lógica de la función no produce efectos colaterales fuera de ella.
*/
function sumaPura (a, b){
  return (a + b);
}

/* FUNCIÓN IMPURA
Devuelve resultados diferente en cada situación pesé a que los parametros de entrada son los mismos.
La lógica de la función modifica efectos externos fuera de ella misma. En el 2do ejemplo cambia el valor de la variable externa.
*/
function sumaImpura(a, b){
  return (a + b + Math.random());
}

let b = 2
function sumaImpura2(a) {
  b = a + b
  return b;
}