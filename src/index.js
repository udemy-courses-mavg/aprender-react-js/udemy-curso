import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';


/**
 * StrictMode. Nos ayuda a asegurarnos que nuesta aplicación siempre sigue las mejores prácticas y que nos será fácil actualizar a nuevas versiones de la librería.
 * Al momento de correr la aplicación otorga información importante mediante warnings, por ejemplo el uso de métodos obsoletos o que estamos generando efectos 
 * secundarios inesperados por usar el state incorrectamente
 */
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
